<?php

date_default_timezone_set('Europe/Riga');

require_once 'StringTimeConversion.php';

// TODO: if no cookies set, check json/history object

$stc = new StringTimeConversion();

$result = false;

if (!empty($_POST)) {
    $logs = $_POST['logs'];
    $logs = explode(PHP_EOL, $logs);

    $ticketUrl = !empty($_POST['ticket_url']) ? trim($_POST['ticket_url'], '/') : false;

    $result = [];

    foreach ($logs as $i => $line) {
        $line = trim($line);

        /**
         * Remove invalid/skippable lines
         */
        $isUnset = false;
        $ignore = [
            '/^\/\/.*/', // Comments
        ];
        foreach ($ignore as $pattern) {
            if (preg_match($pattern, $line)) {
                unset($logs[$i]);
                $isUnset = true;
            }
        }
        if (!strlen($line)) { // Empty lines
            unset($logs[$i]);
            $isUnset = true;
        }

        /**
         * Skip if line is unset
         */
        if ($isUnset) {
            continue;
        }

        /**
         * Collect blocks
         */
        $titlePattern = '/^([A-Za-z]+-[0-9]+)(?=\s-\s(.+))?/';
        if (preg_match($titlePattern, $line, $titleMatches)) {
            $id = $titleMatches[1];
            $title = isset($titleMatches[2]) ? $titleMatches[2] : '';

            if ($ticketUrl) {
                $idUrl = $ticketUrl . '/' . $id;
            } else {
                $idUrl = null;
            }

            $result[$id]['id'] = $id;
            $result[$id]['idUrl'] = $idUrl;
            $result[$id]['title'] = $title;
            $result[$id]['time']['seconds'] = 0;
            $result[$id]['time']['string'] = '0s';
            continue;
        }

        /**
         * Append time and details
         */
        elseif (isset($id)) {
            $timePattern = '/\[([0-9]{1,2}:[0-9]{2})\s?-\s?([0-9]{1,2}:[0-9]{2}|\?).*\]/';
            if (preg_match($timePattern, $line, $timeMatches)) {
                $timeFrom = $timeMatches[1];
                $timeTo = $timeMatches[2] === '?' ? date('H:i') : $timeMatches[2];
                $timeTotal = strtotime($timeTo) - strtotime($timeFrom);

                $result[$id]['ranges'][] = [
                    'from' => $timeFrom,
                    'to' => $timeTo,
                ];

                $result[$id]['time']['seconds'] += $timeTotal;
                $result[$id]['time']['string'] = $stc->getStringFromSeconds(
                    $result[$id]['time']['seconds']
                );

                continue;
            }

            $detailPattern = '/-\s?(.+)/';
            if (preg_match($detailPattern, $line, $detailMatches)) {
                $detail = $detailMatches[1];

                $result[$id]['details'][] = $detail;
                continue;
            }
        }
    }
}

// Store data
if (isset($ticketUrl) && $ticketUrl) {
    setcookie('ticket_url', $ticketUrl, strtotime('+1 year'));
}
if (isset($_POST['logs'])) {
    /**
     * Write the logs to a file and cookies
     */
    if (isset($_POST['save']) && $_POST['save'] == true) {
        setcookie('logs', $_POST['logs'], strtotime('+1 year'));

        // For some reason we have to add +1 day for strtotime give an accurate timestamp
        $date = strtotime(date('d-m-Y')) + strtotime('+1 day') - time();

        // TXT
        $filename = sprintf('history/%s.txt', $date);
        file_put_contents($filename, $_POST['logs']);

        // JSON
        if (isset($result)) {
            $filename = sprintf('json/%s.json', $date);
            file_put_contents($filename, json_encode($result));
        }
    }
}

header('Content-Type: application/json');
echo json_encode($result);
