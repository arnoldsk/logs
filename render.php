<?php

require_once 'StringTimeConversion.php';

$stc = new StringTimeConversion();

if (!isset($_POST['tickets'])) {
    die('');
}

$tickets = $_POST['tickets'];
$report = isset($_POST['report']) ? $_POST['report'] === 'true' : false;

$totalTime = [];
foreach ($tickets as $ticket) {
    $totalTime[] = $ticket['time']['string'];
}
$totalTime = $stc->getOrderedString(implode(' ', $totalTime));

?>
<div id="tickets">
    <?php foreach ($tickets as $id => $ticket): ?>
        <div class="ticket" id="<?= $id ?>">
            <div class="time"><?= $ticket['time']['string'] ?></div>
            <div class="title">
                <?php if ($ticket['idUrl']): ?>
                    <a href="<?= $ticket['idUrl'] ?>"><?= $id ?></a>
                <?php else: ?>
                    <?= $id ?>
                <?php endif ?>
                <?php if ($ticket['title']): ?>
                    - <?= $ticket['title'] ?>
                <?php endif ?>
            </div>
            <div class="details">
                <?php $detailsText = ''; ?>
                <?php if (isset($ticket['details'])): ?>
                    <?php foreach ($ticket['details'] as $detail) $detailsText .= "- {$detail}\n" ?>
                <?php endif ?>
                <?php if ($report): ?>
                    <?= nl2br($detailsText) ?>
                <?php else: ?>
                    <textarea style="min-height: auto;" onclick="this.select()"><?= trim($detailsText) ?></textarea>
                <?php endif ?>
            </div>
        </div>
    <?php endforeach ?>
</div>

<div id="total">
    <span><?= $totalTime ?></span>
</div>
