<?php

require_once 'global.php';

?>
<?php require_once 'views/top.phtml' ?>
    <main>
        <div class="main-content">
            <form action="parse.php" method="post" autocomplete="off">
                <input type="hidden" name="save" value="true">
                <input type="url" name="ticket_url" placeholder="Jira ticket URL" value="<?= $ticketUrl ?>">
                <div class="ta-wrapper">
                    <textarea name="logs"><?= $logs ?></textarea>
                </div>
                <p>
                    <label><input type="checkbox" name="report"> view as a report</label>
                </p>
                <button type="submit">Submit</button>
                <button type="button" onclick="window.open('history.php');">History</button>
                <button type="button" onclick="window.open('showcase.php');">Showcase</button>
                <button type="button" onclick="window.open('tasks.php');">Tasks</button>
                <button type="button" onclick="window.location = 'theme.php';"><?= $themeList[$theme] ?></button>
            </form>
        </div>
        <div class="main-content">
            <div id="result"></div>
        </div>
        <div class="main-content">
            <div id="timeline"></div>
        </div>
    </main>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        const $form = $('form');
        const $result = $('#result');

        const sortObjectByKeys = (unordered) => {
            const ordered = {};

            Object.keys(unordered).sort().forEach((key) => ordered[key] = unordered[key]);

            return ordered;
        };

        const parse = () => {
            const formData = $form.serialize();
            const report = formData.indexOf('report=on') !== -1;

            $.post($form.attr('action'), formData, (tickets) => {
                if (!tickets) {
                    return false;
                }

                $.post('render.php', { tickets, report }, (html) => {
                    $result.html(html);
                    $result.toggleClass('report', report);

                    // Timeline
                    // TODO: move this to somewhere else
                    let ranges = {};

                    for (const ticket of Object.values(tickets)) {
                        for (const range of ticket.ranges) {
                            const normalizeTime = (time) => /^[0-9]:[0-9]{2}$/.test(time) ? `0${time}` : time;

                            range.from = normalizeTime(range.from);
                            range.to = normalizeTime(range.to);

                            ranges[range.from] = {
                                ...range,
                                ticketId: ticket.id,
                            };
                        }
                    }

                    // Order by key
                    ranges = sortObjectByKeys(ranges);

                    // Render the timelne
                    // TODO: have missing ranges as a separate element to indicate a break etc.
                    $('#timeline').html('');
                    for (const [index, range] of Object.entries(Object.values(ranges))) {
                        const rangeWidth = 100 / Object.keys(ranges).length;
                        const nextRange = Object.values(ranges)[parseInt(index) + 1] || null;
                        const showTo = nextRange ? range.to !== nextRange.from : true;

                        const minutesMissing = nextRange
                            ? (new Date(`1970-01-01 ${nextRange.from}`) - new Date(`1970-01-01 ${range.to}`)) / 1000 / 60
                            : 0;

                        $('#timeline').append(`
                            <div class="range${showTo ? ' has-range-to' : ''}" style="width: ${rangeWidth}%;" data-ticket="${range.ticketId}">
                                <div class="range-ticket">${range.ticketId}</div>
                                <div class="range-bottom">
                                    <div class="range-time range-from">${range.from}</div>
                                    <div class="range-arrow">&rarr;</div>
                                    ${showTo ? `<div class="range-time range-to">${range.to}</div>` : ''}
                                </div>
                            </div>
                        `);

                        if (minutesMissing) {
                            $('#timeline').append(`
                                <div class="range minutes-missing">
                                    <div class="range-bottom">
                                        <div class="minutes">${minutesMissing} min</div>
                                    </div>
                                </div>
                            `);
                        }
                    }
                });
            });
        };

        // Emphasise similar tickets on hovering timeline range
        $(document).on('mouseover', '.range', (e) => {
            const $range = $(e.target).closest('.range');
            const ticketId = $range.data('ticket');

            // Add class too all matching tickets
            $('.range').removeClass('hover');
            $(`.range[data-ticket="${ticketId}"]`).addClass('hover');
        }).on('mouseleave', '.range', () => {
            $('.range').removeClass('hover');
        });

        $form.on('submit', (e) => {
            e.preventDefault();
            parse();
        });

        $form.find('textarea').on('input', parse);

        if (<?= json_encode($logs) ?>.length) {
            parse();
        }
    </script>
<?php require_once 'views/bot.phtml' ?>
