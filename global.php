<?php

$themeList = [
    'dark' => 'Dark theme',
    'light' => 'Light theme',
];
$theme = 'light';
if (isset($_COOKIE['theme']) && array_key_exists($_COOKIE['theme'], $themeList)) {
    $theme = $_COOKIE['theme'];
}

$ticketUrl = isset($_COOKIE['ticket_url']) ? $_COOKIE['ticket_url'] : '';
$logs = isset($_COOKIE['logs']) ? $_COOKIE['logs'] : '';
