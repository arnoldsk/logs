<?php

require_once 'global.php';

?>
<?php require_once 'views/top.phtml' ?>
    <main>
        <div class="main-content">
            <?php $dateRange = range(strtotime('-1 week'), strtotime('tomorrow')) // Again, for some reason gotta add +1 day ?>

            <?php foreach (array_reverse(glob('json/*.json')) as $file): ?>
                <?php $date = rtrim(basename($file), '.json') ?>
                <?php if (!in_array($date, $dateRange)) continue ?>
                <?php $tickets = json_decode(file_get_contents($file)) ?>

                <h2><?= date('F jS, D', $date) ?></h2>
                <?php foreach ($tickets as $ticket): ?>
                    <p>
                        <?php if ($ticket->idUrl): ?>
                            <a href="<?= $ticket->idUrl ?>" target="_blank"><?= $ticket->id ?></a>
                        <?php else: ?>
                            <?= $ticket->id ?>
                        <?php endif ?>
                        - <?= $ticket->title ?>
                    </p>
                <?php endforeach ?>
            <?php endforeach ?>
        </div>
    </main>
<?php require_once 'views/bot.phtml' ?>
