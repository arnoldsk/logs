<?php

require_once 'global.php';

function getDataHours($time) {
    $data = json_decode(file_get_contents('json/' . $time . '.json'));
    $seconds = 0;

    foreach ($data as $task) {
        $seconds += $task->time->seconds;
    }

    return floor($seconds * 100 / 60 / 60) / 100;
}

?>
<?php require_once 'views/top.phtml' ?>
    <main>
        <div class="main-content">
            <?php foreach (array_reverse(glob('history/*.txt')) as $file): ?>
                <?php $time = rtrim(basename($file), '.txt') ?>
                <?php $hours = getDataHours($time) ?>
                <a class="history-item" href="<?= $file ?>">
                    <span class="date"><?= date('F jS, D', $time) ?></span>
                    <small>(<?= $hours ?>h)</small>
                    <textarea class="logs" style="display: none;"><?= file_get_contents($file) ?></textarea>
                </a>
            <?php endforeach ?>
        </div>
        <div class="main-content" id="result"></div>
    </main>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        $(() => {
            $('.history-item').on('click', function(e) {
                e.preventDefault();

                $btn = $(this);
                logs = $btn.find('.logs').val();
                $result = $('#result');

                $('.history-item').removeClass('active');
                $btn.addClass('active');

                $.post('parse.php', { logs }, (tickets) => {
                    $.post('render.php', { tickets }, (html) => {
                        $result.html(html);
                    });
                });
            });
        });
    </script>
<?php require_once 'views/bot.phtml' ?>
