<?php

class StringTimeConversion
{
    protected $types = array(
        "s" => "seconds",
        "m" => "minutes",
        "h" => "hours",
        "d" => "days",
        "w" => "weeks",
        "M" => "months",
        "y" => "years",
    );

    /**
     * Self explanatory function
     * Checks if sting is valid for
     * using with other functions
     *
     * @return bool
     **/
    public function validateString($string)
    {
        foreach (explode(" ", preg_replace("/\s+/", " ", $string)) as $part) {
            $number = mb_substr($part, 0, -1);
            $abbr = mb_substr($part, -1);

            if (empty($number) || is_int($abbr) || !in_array($abbr, array_keys($this->types))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Combines two other methods and returns
     * reordered and converted string of time
     * E.G. "630m 5000s" becomes "11h 53m 20s"
     *
     * @return string
     **/
    public function getOrderedString($input)
    {
        return $this->getStringFromSeconds($this->getSecondsFromString($input));
    }

    /**
     * Converts a string into seconds
     * E.G. "2m 10s" becomes 130
     *
     * @return int
     **/
    public function getSecondsFromString($string)
    {
        $parts = array();

        foreach (explode(" ", preg_replace("/\s+/", " ", $string)) as $part) {
            foreach ($this->types as $abbr => $type) {
                if (mb_strpos($part, $abbr) !== false) $parts[] = str_replace($abbr, $type, $part);
            }
        }

        return strtotime(implode(" ", $parts), 0);
    }

    /**
     * Converts seconds into string
     * E.G. 130 becomes "2m 10s"
     *
     * @return string
     **/
    public function getStringFromSeconds($seconds)
    {
        $dt1 = new \DateTime("@0");
        $dt2 = new \DateTime("@{$seconds}");

        $string = $dt1->diff($dt2)->format(" %yy %mm %dd %hh %im %ss");

        return trim(preg_replace("/\s0./", "", $string));
    }
}
