<?php

require_once 'global.php';

$jsonFiles = array_reverse(glob('json/*.json'));
$tasks = [];

foreach ($jsonFiles as $file) {
    $data = json_decode(file_get_contents($file));

    foreach ($data as $task) {
        $tasks[$task->id] = $task->title;
    }
}

?>
<?php require_once 'views/top.phtml' ?>
    <main>
        <div class="main-content">
            <?php foreach ($tasks as $id => $title): ?>
                <p><?= $id ?> - <?= $title ?></p>
            <?php endforeach ?>
        </div>
    </main>
<?php require_once 'views/bot.phtml' ?>
