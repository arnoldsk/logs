<?php

require_once 'global.php';

$newTheme = 'dark';
if ($theme === 'dark') {
    $newTheme = 'light';
}

setcookie('theme', $newTheme, strtotime('+1 year'));

header('Location: index.php');
